; Trivial Blinker Test code for PIC 18F27K42
;
; Author : Hans Kramer
;
; Deta   : Jan 2022
    
; PIC18F27K42 Configuration Bit Settings

; Assembly source line config statements

; CONFIG1L
  CONFIG  FEXTOSC = OFF         ; External Oscillator Selection (Oscillator not enabled)
  CONFIG  RSTOSC = EXTOSC       ; Reset Oscillator Selection (EXTOSC operating per FEXTOSC bits (device manufacturing default))

; CONFIG1H
  CONFIG  CLKOUTEN = OFF        ; Clock out Enable bit (CLKOUT function is disabled)
  CONFIG  PR1WAY = ON           ; PRLOCKED One-Way Set Enable bit (PRLOCK bit can be cleared and set only once)
  CONFIG  CSWEN = ON            ; Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)
  CONFIG  FCMEN = ON            ; Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor enabled)

; CONFIG2L
  CONFIG  MCLRE = EXTMCLR       ; MCLR Enable bit (If LVP = 0, MCLR pin is MCLR; If LVP = 1, RE3 pin function is MCLR )
  CONFIG  PWRTS = PWRT_OFF      ; Power-up timer selection bits (PWRT is disabled)
  CONFIG  MVECEN = ON           ; Multi-vector enable bit (Multi-vector enabled, Vector table used for interrupts)
  CONFIG  IVT1WAY = ON          ; IVTLOCK bit One-way set enable bit (IVTLOCK bit can be cleared and set only once)
  CONFIG  LPBOREN = OFF         ; Low Power BOR Enable bit (ULPBOR disabled)
  CONFIG  BOREN = OFF           ; Brown-out Reset Enable bits (Brown-out Reset disabled)

; CONFIG2H
  CONFIG  BORV = VBOR_2P45      ; Brown-out Reset Voltage Selection bits (Brown-out Reset Voltage (VBOR) set to 2.45V)
  CONFIG  ZCD = OFF             ; ZCD Disable bit (ZCD disabled. ZCD can be enabled by setting the ZCDSEN bit of ZCDCON)
  CONFIG  PPS1WAY = ON          ; PPSLOCK bit One-Way Set Enable bit (PPSLOCK bit can be cleared and set only once; PPS registers remain locked after one clear/set cycle)
  CONFIG  STVREN = ON           ; Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
  CONFIG  DEBUG = OFF           ; Debugger Enable bit (Background debugger disabled)
  CONFIG  XINST = OFF           ; Extended Instruction Set Enable bit (Extended Instruction Set and Indexed Addressing Mode disabled)

; CONFIG3L
  CONFIG  WDTCPS = WDTCPS_31    ; WDT Period selection bits (Divider ratio 1:65536; software control of WDTPS)
  CONFIG  WDTE = OFF            ; WDT operating mode (WDT Disabled; SWDTEN is ignored)

; CONFIG3H
  CONFIG  WDTCWS = WDTCWS_7     ; WDT Window Select bits (window always open (100%); software control; keyed access not required)
  CONFIG  WDTCCS = SC           ; WDT input clock selector (Software Control)

; CONFIG4L
  CONFIG  BBSIZE = BBSIZE_512   ; Boot Block Size selection bits (Boot Block size is 512 words)
  CONFIG  BBEN = OFF            ; Boot Block enable bit (Boot block disabled)
  CONFIG  SAFEN = OFF           ; Storage Area Flash enable bit (SAF disabled)
  CONFIG  WRTAPP = OFF          ; Application Block write protection bit (Application Block not write protected)

; CONFIG4H
  CONFIG  WRTB = OFF            ; Configuration Register Write Protection bit (Configuration registers (300000-30000Bh) not write-protected)
  CONFIG  WRTC = OFF            ; Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
  CONFIG  WRTD = OFF            ; Data EEPROM Write Protection bit (Data EEPROM not write-protected)
  CONFIG  WRTSAF = OFF          ; SAF Write protection bit (SAF not Write Protected)
  CONFIG  LVP = ON              ; Low Voltage Programming Enable bit (Low voltage programming enabled. MCLR/VPP pin function is MCLR. MCLRE configuration bit is ignored)

; CONFIG5L
  CONFIG  CP = OFF              ; PFM and Data EEPROM Code Protection bit (PFM and Data EEPROM code protection disabled)

; CONFIG5H

// config statements should precede project file includes.
#include <xc.inc>

#define DELAY_INDEX(o)   (delay_counter+o)


delay_counter  EQU  0 ; takes up 4 bytes


INIT_DELAY MACRO dest
    INCF   dest, A
    INCF   dest+1, A
    INCF   dest+2, A
    INCF   dest+3, A
ENDM
  
DELAY MACRO imm32
    MOVLW imm32 AND 0xff
    MOVWF delay_counter, A

    MOVLW (imm32 >> 8) AND 0xff
    MOVWF delay_counter+1, A

    MOVLW (imm32 >> 16) AND 0xff
    MOVWF delay_counter+2, A

    MOVLW (imm32 >> 24) AND 0xff
    MOVWF delay_counter+3, A
    CALL      delay
ENDM

PSECT reset_vector, class=CODE, delta=1
reset_vector:
    GOTO    main
    

PSECT code, delta=1
main:
    BANKSEL PORTA
    CLRF    PORTA, 0
    BANKSEL LATA
    CLRF    LATA, 0
    BANKSEL ANSELA
    CLRF    ANSELA, 0
    BANKSEL TRISA
    MOVLW   0b00000000
    MOVWF   TRISA, 1
    
    BANKSEL PORTA
main_1:
    CLRF    PORTA, 0
    DELAY   40000
    SETF    PORTA, 0
    DELAY   40000
    NOP
    BRA     main
 

delay:
    INIT_DELAY delay_counter

delay_0:
    DECFSZ DELAY_INDEX(0), A
    GOTO   delay_0

    DECFSZ DELAY_INDEX(1), A
    GOTO   delay_0

    DECFSZ DELAY_INDEX(2), A
    GOTO   delay_0

    DECFSZ DELAY_INDEX(3), A
    GOTO   delay_0

    RETURN
 
END reset_vector

