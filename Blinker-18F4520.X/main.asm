PROCESSOR 18F4520
    
#include <xc.inc>
#include "pic18f4520.inc"


; CONFIG1H
  CONFIG  OSC = INTIO67          ; Oscillator Selection bits (Internal oscillator block, CLKO function on RA6, port function on RA7)
  CONFIG  FCMEN = OFF           ; Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)
  CONFIG  IESO = OFF            ; Internal/External Oscillator Switchover bit (Oscillator Switchover mode disabled)

; CONFIG2L
  CONFIG  PWRT = OFF            ; Power-up Timer Enable bit (PWRT disabled)
  CONFIG  BOREN = SBORDIS       ; Brown-out Reset Enable bits (Brown-out Reset enabled in hardware only (SBOREN is disabled))
  CONFIG  BORV = 3              ; Brown Out Reset Voltage bits (Minimum setting)

; CONFIG2H
  CONFIG  WDT = OFF             ; Watchdog Timer Enable bit (WDT disabled (control is placed on the SWDTEN bit))
  CONFIG  WDTPS = 32768         ; Watchdog Timer Postscale Select bits (1:32768)

; CONFIG3H
  CONFIG  CCP2MX = PORTC        ; CCP2 MUX bit (CCP2 input/output is multiplexed with RC1)
  CONFIG  PBADEN = OFF          ; PORTB A/D Enable bit (PORTB<4:0> pins are configured as analog input channels on Reset)
  CONFIG  LPT1OSC = OFF         ; Low-Power Timer1 Oscillator Enable bit (Timer1 configured for higher power operation)
  CONFIG  MCLRE = ON            ; MCLR Pin Enable bit (MCLR pin enabled; RE3 input pin disabled)

; CONFIG4L
  CONFIG  STVREN = ON           ; Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
  CONFIG  LVP =   OFF            ; Single-Supply ICSP Enable bit (Single-Supply ICSP enabled)
  CONFIG  XINST = OFF           ; Extended Instruction Set Enable bit (Instruction set extension and Indexed Addressing mode disabled (Legacy mode))

; CONFIG5L
  CONFIG  CP0 = OFF             ; Code Protection bit (Block 0 (000800-001FFFh) not code-protected)
  CONFIG  CP1 = OFF             ; Code Protection bit (Block 1 (002000-003FFFh) not code-protected)
  CONFIG  CP2 = OFF             ; Code Protection bit (Block 2 (004000-005FFFh) not code-protected)
  CONFIG  CP3 = OFF             ; Code Protection bit (Block 3 (006000-007FFFh) not code-protected)

; CONFIG5H
  CONFIG  CPB = OFF             ; Boot Block Code Protection bit (Boot block (000000-0007FFh) not code-protected)
  CONFIG  CPD = OFF             ; Data EEPROM Code Protection bit (Data EEPROM not code-protected)

; CONFIG6L
  CONFIG  WRT0 = OFF            ; Write Protection bit (Block 0 (000800-001FFFh) not write-protected)
  CONFIG  WRT1 = OFF            ; Write Protection bit (Block 1 (002000-003FFFh) not write-protected)
  CONFIG  WRT2 = OFF            ; Write Protection bit (Block 2 (004000-005FFFh) not write-protected)
  CONFIG  WRT3 = OFF            ; Write Protection bit (Block 3 (006000-007FFFh) not write-protected)

; CONFIG6H
  CONFIG  WRTC = OFF            ; Configuration Register Write Protection bit (Configuration registers (300000-3000FFh) not write-protected)
  CONFIG  WRTB = OFF            ; Boot Block Write Protection bit (Boot block (000000-0007FFh) not write-protected)
  CONFIG  WRTD = OFF            ; Data EEPROM Write Protection bit (Data EEPROM not write-protected)

; CONFIG7L
  CONFIG  EBTR0 = OFF           ; Table Read Protection bit (Block 0 (000800-001FFFh) not protected from table reads executed in other blocks)
  CONFIG  EBTR1 = OFF           ; Table Read Protection bit (Block 1 (002000-003FFFh) not protected from table reads executed in other blocks)
  CONFIG  EBTR2 = OFF           ; Table Read Protection bit (Block 2 (004000-005FFFh) not protected from table reads executed in other blocks)
  CONFIG  EBTR3 = OFF           ; Table Read Protection bit (Block 3 (006000-007FFFh) not protected from table reads executed in other blocks)

; CONFIG7H
  CONFIG  EBTRB = OFF           ; Boot Block Table Read Protection bit (Boot block (000000-0007FFh) not protected from table reads executed in other blocks)


#define DELAY_INDEX(o)   (delay_counter+o)
 
delay_counter  EQU  0 ; leave 4 bytes!
pattern        EQU  4 ; 
index          EQU 20

  
INIT_DELAY MACRO dest
   INCF dest, A
   INCF dest+1, A
   INCF dest+2, A
   INCF dest+3, A
ENDM
 

MOVL32F32 MACRO imm32, dest
    MOVLW imm32 AND 0xff
    MOVWF dest, A
    
    MOVLW (imm32 >> 8) AND 0xff
    MOVWF dest+1, A
    
    MOVLW (imm32 >> 16) AND 0xff
    MOVWF dest+2, A
    
    MOVLW (imm32 >> 24) AND 0xff
    MOVWF dest+3, A
ENDM   

DELAY MACRO imm32
    MOVL32F32 imm32, delay_counter
    CALL      delay
ENDM

     
PSECT reset_vector, class=CODE, delta=1
reset_vector:
      PAGESEL main
      GOTO    main

PSECT int_vector, class=CODE
int_vector:
     RETFIE

PSECT code, delta=1
main:   
    MOVLW 0b11110010   
    MOVWF OSCCON, A
    
    MOVLW 0x07
    MOVWF ADCON1, A 
    
    MOVLW 0x07 
    MOVWF CMCON, A 
 
    CLRF  TRISA, A
    CLRF  TRISB, A
    CLRF  TRISC, A
    CLRF  TRISD, A
    
    MOVLW 0b00000001
    MOVWF pattern, A
    MOVWF pattern+1, A
    
    MOVLW 0b00000010
    MOVWF pattern+2, A
    MOVWF pattern+3, A
    
    MOVLW 0b00000100
    MOVWF pattern+4, A
    MOVWF pattern+5, A
    
    MOVLW 0b00001000
    MOVWF pattern+6, A 
    MOVWF pattern+7, A
    
    MOVLW 0b00010000
    MOVWF pattern+8, A
    MOVWF pattern+9, A
    
    MOVLW 0b00100000
    MOVWF pattern+10, A
    MOVWF pattern+11, A
    
    MOVLW 0b10000000
    MOVWF pattern+12, A
    MOVLW 0b01000000
    MOVWF pattern+13, A
    
    MOVLW 0b01000000
    MOVWF pattern+14, A
    MOVLW 0b10000000
    MOVWF pattern+15, A
    
    
main1:
    LFSR  0, pattern
    MOVLW 0x08
    MOVWF index, A
    
main2:    
    MOVF  POSTINC0, W, A
    MOVWF LATA, A
    MOVF  POSTINC0, W, A
    MOVWF LATB, A
    MOVWF LATC, A
    
    DELAY     33333
    
    DECFSZ index, A
    BRA    main2
    
    GOTO   main1
 
delay:
     INIT_DELAY delay_counter
    
delay_0:
      DECFSZ DELAY_INDEX(0), A
      GOTO   delay_0
      
      DECFSZ DELAY_INDEX(1), A
      GOTO   delay_0
      
      DECFSZ DELAY_INDEX(2), A
      GOTO   delay_0
      
      DECFSZ DELAY_INDEX(3), A
      GOTO   delay_0
      
      RETURN
 
END reset_vector
