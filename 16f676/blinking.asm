;
;   \Author   Hans Kramer
;
;   \Date     June 2018
;

                list         p=16F676
        
                include     "p16f676.inc"
        
                errorlevel  -302
        
                __config    _CP_OFF & _CPD_OFF & _BODEN_OFF & _MCLRE_OFF & _WDT_OFF & _PWRTE_ON & _INTRC_OSC_NOCLKOUT
        
        
                ; general purpose registers
                cblock  0x20        
                    w_temp
                    status_temp
        
                    count1          
                    count2          
                    count3          
                endc
    

                org      0x0000          
                goto     main                    

                ; interrupt vector location
                org      0x004

                ; store W and status register
                movwf    w_temp
                movf     STATUS, w
                movwf    status_temp

                ; isr code can go here or be located as a call subroutine elsewhere

                movf    status_temp,w     ; retrieve copy of STATUS register
                movwf   STATUS            ; restore pre-isr STATUS register contents
                swapf   w_temp,f
                swapf   w_temp,w          ; restore pre-isr W register contents

                retfie

main
                ; select bank 0
                bcf      STATUS, RP0  
                
                ; initialize PORTA
                clrf     PORTA       ;Init PORTA

                ; configure comparator
                movlw    05h
                movwf    CMCON

                ; select bank 1
                bsf      STATUS, RP0 

                ; select digital I/O
                clrf     ANSEL       

                ; all pins as output
                movlw    b'00000000'
                movwf    TRISC
                movwf    TRISA

                ; select bank 0
                bcf      STATUS, RP0 ;select bank 0


loop    
                ; set pins high
                movlw   b'11111111'
                movwf   PORTA 
                movwf   PORTC
           
                call    delay

                ; set pins low
                movlw   b'00000000'
                movwf   PORTA
                movwf   PORTC

                call    delay
                goto    loop

 
                ;delay 250 ms (4 MHz clock)
delay           movlw   d'250'          
                movwf   count1
delay0          movlw   0xc7
                movwf   count2
                movlw   0x01
                movwf   count3
delay1
                nop
                nop
                decfsz  count2, f
                goto    delay1
                decfsz  count3, f
                goto    delay1
            
                decfsz  count1, f
                goto    delay0
                return
           
                end
